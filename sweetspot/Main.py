# import necessary modules
import tkinter as tk
from tkinter import messagebox
from PIL import ImageTk, Image, ImageDraw, ImageFont
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import matplotlib.pyplot as plt

# Model Functions
#Moment of Inertia
def Ikf(T,mb,com):
    return (mb*9.8*com*(T**2))/(4*(np.pi**2))

#Radius of gyration squared
def ko2f(Ik,mb):
    return  Ik/mb

#force of ball on bat
def Fbf(mb, vp, vout):
    return (mb*(vout+vp))/0.007

#Transverse Force
def Ftf(x,com,ko2, Fb):
    return (1-(x/(ko2/com)))*Fb

#Kinematic factor
def kinf (x,Ik):
    #Average mass of baseball (kg)
    mball=0.145
    return ((mball*((x)**2))/Ik)

#collision efficiency
def eaff(x,Ik):
    #Recoil factor
    k=kinf(x, Ik)
    #BBCOR Certification of Baseball Bats
    e=0.5
    return (e-k)/(1+k)

#batted ball speed (vf of ball)
def BBSf(x,vp,wb,Ik):
    ea=eaff(x,Ik)
    return ea*vp+((1+ea)*(wb*x))

#Center of Percussion (sweetspot)
def copf(ko2,com):
    return ko2/com

#GUI Functions
# clears bat specification entries
def clear():
    lbe.delete(0, "end")
    mbe.delete(0, "end")
    come.delete(0, "end")
    Te.delete(0, "end")
    wbe.delete(0, "end")
    vpe.delete(0, "end")

# Finds Sweet Spot, and creates graphs for BBS and transverse force
def simulatess(T, mb, com, lb, wb, vp):
    # Moment of Inertia about Handle (pivot point)
    Io = Ikf(T, mb, com)

    # Radius of gyration squared
    ko2 = ko2f(Io, mb)

    # Center of Percussion
    cop = copf(ko2, com) * 39.37

    # Array of Locations that Baseball makes contact with bat
    x = np.linspace(0, lb, 100000)
    xin = x * 39.3701

    # Run Models for forces and BBS
    BBS = BBSf(x, vp, wb, Io)
    BBSmph = BBS * 2.24
    Fb = Fbf(mb, vp, BBS)
    Ft = Ftf(x, com, ko2, Fb)

    # Add center of percussion to Data arrays
    idx = int((np.abs(Ft - 0)).argmin())
    Fta = Ft.tolist()
    xina = xin.tolist()
    BBSmpha = BBSmph.tolist()
    if Ft[idx] < 0:
        xina.insert(idx + 1, cop)
        Fta.insert(idx + 1, 0)
        BBSmpha.insert(idx + 1, BBSf((cop / 39.3701), vp, wb, Io) * 2.24)
    if Ft[idx] > 0:
        xina.insert(idx, cop)
        Fta.insert(idx, 0)
        BBSmpha.insert(idx, BBSf((cop / 39.3701), vp, wb, Io) * 2.24)
    xin = np.array(xina)
    Ft = np.array(Fta)
    BBSmph = np.array(BBSmpha)

    # plot Transverse Force
    fig1, (ax1, ax2) = plt.subplots(1, 2, gridspec_kw={'width_ratios': [1, 1]})
    ax1.plot(xin, Ft)
    # label axis
    ax1.set(xlabel='Location of Contact (in)', ylabel='$F_t$ (N)', title="Transverse Force vs Location of Contact")
    ax1.grid()

    # Plot BBS (exit speed of ball)
    ax2.plot(xin, BBSmph)
    # label axis
    ax2.set(xlabel='Location of Contact (in)', ylabel='BBS (mph)', title="Batted Ball Speed vs Location of Contact")
    ax2.grid()
    fig1.set_size_inches(10, 3)
    fig1.tight_layout()

    return cop, fig1, xin, BBSmph, Ft

# Function creates Display window with initial simulation results
def Display():

    # Function runs simulation to calculate transverse force and BBS at specified location
    def minisim():
        try:
            lp = float(x.get())
        except:
            messagebox.showerror("Error", "Please input numerical value for length of bat")
            return
        if float(x.get())>(lb/0.0254):
            messagebox.showerror("Error", "Please input value<length of bat")
            return
        if float(x.get())<0:
            messagebox.showerror("Error", "Please input value>0")
            return
        idx = (np.abs(xin - lp)).argmin()
        pop = tk.Tk()
        pop.title("Ft and BBS".format(lp))
        tk.Label(pop, text="At Contact Point: {:.2f} in".format(lp)).pack()
        tk.Label(pop, text="Transverse Force = {:.2f} N".format(Ft[idx])).pack()
        tk.Label(pop, text="Batted Ball Speed = {:.2f} mph".format(BBSmph[idx])).pack()

    #Extract bat specifications
    try:
        lb = float(lbe.get()) * 0.0254
    except:
        messagebox.showerror("Error", "Please input numerical value for length of bat")
        return
    if lb<=0:
        messagebox.showerror("Error", "Please input value >0 in for length of bat")
        return
    try:
        mb = float(mbe.get()) * 0.0283495
    except:
        messagebox.showerror("Error", "Please input numerical value for mass of bat")
        return
    if mb<=0:
        messagebox.showerror("Error", "Please input value >0 kg for mass of bat")
        return
    try:
        com = float(come.get()) * 0.0254
    except:
        messagebox.showerror("Error", "Please input numerical value for center of mass of bat")
        return
    if com<=0:
        messagebox.showerror("Error", "Please input value >0 in for center of mass of bat")
        return
    try:
        T = float(Te.get())
    except:
        messagebox.showerror("Error", "Please input numerical value for period of oscillation")
        return
    if T<=0:
        messagebox.showerror("Error", "Please input value >0 sec for period of oscillation of bat")
        return
    try:
        wb = (float(wbe.get()) * 0.44704) / lb
    except:
        messagebox.showerror("Error", "Please input numerical value for swing speed")
        return
    if wb<=0:
        messagebox.showerror("Error", "Please input value >0 sec for swing speed")
        return
    try:
        vp = float(vpe.get()) * 0.44704
    except:
        messagebox.showerror("Error", "Please input numerical value for swing speed")
        return
    if vp<=0:
        messagebox.showerror("Error", "Please input value >0 sec for pitch speed")
        return

    # Run Simulation with given bat specifications
    ss, fig1, xin, BBSmph, Ft = simulatess(T, mb, com, lb, wb, vp)
    if T>np.sqrt((4*(np.pi**2))/9.8)*lb:
        res = messagebox.askquestion("Warning", "Model has detected sweetspot outside the length of your bat, click yes to run simulation with current data or no to re-enter specifications. (Hint: in order for sweetspot to be on the bat, the following inequality must be held: Period < sqrt(4*pi^2/9.8)*bat length")
        if res=="no":
            return
    # Create new window to show simulation results
    Window = tk.Toplevel()

    # Create Label to tell user sweet spot of the bat
    sslabel = tk.Label(Window, text="The sweet spot of your bat is {:.2f} inches from the knob.".format(ss))
    sslabel.pack()
    BBSmaxloc=xin[np.where(BBSmph==max(BBSmph))]
    BBSlabel = tk.Label(Window, text="The batted ball speed is max when ball strikes {:.2f} inches from the knob.".format(BBSmaxloc[0]))
    BBSlabel.pack()
    # Open image of bat, paste ball on sweetspot, and place image on canvas
    bat = Image.open("BaseballBat.png")
    bull = Image.new("L", [45, 45], color="white")
    bulld = ImageDraw.Draw(bull)
    font = ImageFont.truetype("/Library/Fonts/Arial.ttf", size=20)
    bulld.text([10, 12], "SS", fill="black", font=font)
    im = Image.new("L", [45, 45], color=(000))
    draw = ImageDraw.Draw(im)
    draw.ellipse((5, 5, 40, 40), fill='white',outline="black")
    resized_bat = bat.resize((500, 75), Image.ANTIALIAS)
    # Calculate location of sweetspot on baseball bat image
    ssim = round((ss / (lb * 39.37)) * 500)
    resized_bat.paste(bull, [ssim - 23, 14], im)
    batrs = ImageTk.PhotoImage(resized_bat)
    batpic = tk.Label(Window, image=batrs)
    batpic.pack()

    # Pack graphs from simulation onto window
    canvas = FigureCanvasTkAgg(fig1, master=Window)
    canvas.draw()

    # placing graphs on the Tkinter window
    canvas.get_tk_widget().pack()

    # Create label for text entry field for specifying ball contact location
    tk.Label(Window, text="Please Enter Contact Location (in):").pack(side="left")

    # Create text entry field for specified ball contact location
    x = tk.Entry(Window)

    # Set default value to sweet spot calculated
    x.insert(0, "{:f}".format(ss))
    x.pack(side="left")

    # Create button to find ball speed and transverse force at specified location
    loc = tk.Button(Window, text="Find Force and BBS at Specified Contact Location", bg='White', fg='Black',
                    command=minisim)
    loc.pack(side="left")
    new = tk.Button(Window, text="Find New Sweet Spot", bg='White', fg='Black', command=Window.destroy)
    new.pack(side="right")

# Main Script

# Create Tkinter Window
ws = tk.Tk()

# Title Window
ws.title("Sweet Spot Finder")

# Create labels for text entry fields
tk.Label(ws, text="Please Enter Bat Specifications").grid(row=0, columnspan=2)
tk.Label(ws, text="Bat Length (in):").grid(row=1)
tk.Label(ws, text="Bat Weight (oz):").grid(row=2)
tk.Label(ws, text="Center of Mass [Balance Point](in):").grid(row=3)
tk.Label(ws, text="Period of Oscillation (sec):").grid(row=4)
tk.Label(ws, text="Swing Speed (mph):").grid(row=5)
tk.Label(ws, text="Pitch Speed (mph):").grid(row=6)

# Create text entry fields and fill with Default Values (Typicall MLB Baseball bat values)
lbe = tk.Entry(ws)
lbe.insert(0, "34")
mbe = tk.Entry(ws)
mbe.insert(0, "31")
come = tk.Entry(ws)
come.insert(0, "20.7")
Te = tk.Entry(ws)
Te.insert(0, "1.72")
wbe = tk.Entry(ws)
wbe.insert(0, "85")
vpe = tk.Entry(ws)
vpe.insert(0, "93")

# Place Entry fields on Grid
lbe.grid(row=1, column=1)
mbe.grid(row=2, column=1)
come.grid(row=3, column=1)
Te.grid(row=4, column=1)
wbe.grid(row=5, column=1)
vpe.grid(row=6, column=1)

# Create Buttons to Run Simulation and Clear Entry Fields
find = tk.Button(ws, text="Find Sweet Spot", bg='White', fg='Black', command=Display)
clear = tk.Button(ws, text="Clear Entries", bg='White', fg='Black', command=clear)

# Place Simulation and Clear Buttons on the grid
find.grid(row=7, column=0)
clear.grid(row=7, column=1)

# Run GUI
ws.mainloop()

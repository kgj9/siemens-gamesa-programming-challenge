# Siemens Gamesa Programming Challenge

This GUI takes the specifications of a baseball bat, pitching speed, and bat speed as the user input, then outputs the location of the sweet spot on the bat (the center of percussion), location of max BBS, and plots the transverse force felt by the batter and the BBS as a function of contact location.
